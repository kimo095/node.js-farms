const http = require('http');
const url = require('url');
const fs = require('fs');

//import  from other module

const replaceTemplate = require('./replaceTemplate');


//here in this case we use Sync way, because we need our data to be read only once

const overView = fs.readFileSync(`${__dirname}/overview.html`, 'utf-8');
const productHTML = fs.readFileSync(`${__dirname}/product.html`, 'utf-8');
const card = fs.readFileSync(`${__dirname}/template-card.html`, 'utf-8');

const data = fs.readFileSync(`${__dirname}/data.json`, 'utf-8');
const dataObj = JSON.parse(data);
// let dataObj;
// try {
//     dataObj = JSON.parse(data);
// } catch (error) {
//     console.error('Error parsing data.json:', error);
//     dataObj = []; // or any default value you want to assign
// }

const server = http.createServer((req, res) => {
    const { query, pathname } = url.parse(req.url, true);

    //overview page
    if (pathname === '/' || pathname === '/overview') {
        res.writeHead(200, { 'Content-type': 'text/html' });
        const cardHtml = dataObj.map(el => replaceTemplate(card, el)).join('');
        const output = overView.replace('{%PRODUCTS_CARD%}', cardHtml);
        res.end(output);
    }
    //products page
    else if (pathname === '/product') {
        res.writeHead(200, { 'Content-type': 'text/html' });
        const product = dataObj[query.id];
        const output = replaceTemplate(productHTML , product)
        res.end(output);
    } else if (pathname === '/api') {
        res.writeHead(200, {
            'Content-type': 'application/json'
        });
        res.end(data);
    } else {
        res.writeHead(404, {
            'Content-type': 'text/html',
            'my-own-header': "hoie hoi"
        });
        res.end('<h1>Page Not found</h1>');
    }
}

);

server.listen(8000, '127.0.0.1', () => {
    console.log('Listening to the request from the port 8000');
});
